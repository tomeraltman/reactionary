FROM continuumio/miniconda

MAINTAINER Tomer Altman, Altman Analytics LLC

Workdir /root


### Install apt dependencies

RUN DEBIAN_FRONTEND=noninteractive apt-get update --yes
RUN DEBIAN_FRONTEND=noninteractive apt-get install --yes --fix-missing \
    make \
    python3 \
    python3-pip \
    procps \
    sqlite3 \
    texlive-latex-recommended \
    texlive-fonts-recommended \
    tex-gyre \
    texlive-latex-extra \
    latexmk


### Copy over files:
COPY reactionary /root/reactionary/reactionary
COPY bin         /root/reactionary/bin
COPY doc         /root/reactionary/doc
COPY test        /root/reactionary/test
COPY Makefile    /root/reactionary/
COPY MANIFEST.in /root/reactionary/
COPY README.md   /root/reactionary/
COPY setup.py    /root/reactionary/


### Fetch reference database:
#ADD https://ndownloader.figshare.com/files/25440413?private_link=9f94fa9056cdbb4f0b83 /root/reactionary/ref-data/nog2rxn_v6.gz

### Install package:
RUN cd /root/reactionary && make install-package

### Setup the environment:
ENV PATH=$PATH:/root/.local/bin