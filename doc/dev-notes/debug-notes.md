# False Positive debugging of E. coli 2020-12-03

Stats:
 # prots perfect prediction: 3178
 # prots with FP predictions only: 857
 # prots with FN predictions only: 66
 # prots with both FP and FN predictions: 37
 
ten random proteins from list of 857 FP-only proteins:
'G6646-MONOMER', 'FLAVODOXIN2-MONOMER', 'G6827-MONOMER', 'SGCA-MONOMER', 'G7093-MONOMER', 'EG11134-MONOMER', 'G7186-MONOMER', 'EG12624-MONOMER', 'MONOMER0-4519'

# Report for protein G6646-MONOMER
## Annotation:
Annot(prot_id='G6646-MONOMER', description='Belongs to the class-III pyridoxal-phosphate-dependent aminotransferase family', ecs=['2.6.1.19', '2.6.1.22'], eggnog_ids=['1MWY6@1224', '1RMP0@1236', '3XNUA@561', 'COG0160@1', 'COG0160@2'])

## PGDB Stats:
### Direct Reactions of Protein:
- GABATRANSAM-RXN,VAGL-RXN

## Predictions
### Predicted Reactions:
--- VAGL-RXN,GABATRANSAM-RXN,ACETYLORNTRANSAM-RXN
(['GABATRANSAM-RXN', 'VAGL-RXN'], ['VAGL-RXN', 'GABATRANSAM-RXN', 'ACETYLORNTRANSAM-RXN'])

G6646-MONOMER conclusion:
	Protein Puue; has possible paralog GabT that catalyzes all three reactions. Since same protein family (EggNOG annotats the protein as GabT), hard to differentiate the specificity. Erring on the side of more promiscuity, but alerting the user, seems to be the way to go.

%%%

# Report for protein FLAVODOXIN2-MONOMER
## Annotation:
Annot(prot_id='FLAVODOXIN2-MONOMER', description='Low-potential electron donor to a number of redox enzymes', ecs=[''], eggnog_ids=['1QRBW@1224', '1RMED@1236', '3XMVZ@561', 'COG0716@1', 'COG0716@2'])

## PGDB Stats:
### Direct Reactions of Protein:
- 

## Predictions
### Predicted Reactions:
--- R343-RXN

Conclusion:

This is an electron carrier protein. 'COG0716@2' did fetch a reaction. So issue with how far we climb up the protein tree trying to find a match. Possible solution: flag these situations to the user, to allow them to judge the quality themselves.

%%%

# Report for protein G6827-MONOMER
## Annotation:
Annot(prot_id='G6827-MONOMER', description='Corresponds to locus_tag', ecs=['3.2.1.17'], eggnog_ids=['1RA0W@1224', '1S2XG@1236', '3XQB7@561', 'COG3772@1', 'COG3772@2'])

## PGDB Stats:
### Direct Reactions of Protein:
- 

## Predictions
### Predicted Reactions:
--- 3.2.1.17-RXN

Conclusion:
EcoCyc says no function for this protein. Again, found annotation at level of 'COG3772@2'.

%%%

# Report for protein SGCA-MONOMER
## Annotation:
Annot(prot_id='SGCA-MONOMER', description='The phosphoenolpyruvate-dependent sugar phosphotransferase system (sugar PTS), a major carbohydrate active -transport system, catalyzes the phosphorylation of incoming sugar substrates concomitantly with their translocation across the cell membrane', ecs=['2.7.1.194', '2.7.1.195', '2.7.1.197', '2.7.1.200', '2.7.1.202', '2.7.3.9'], eggnog_ids=['1RKN3@1224', '1S73Z@1236', 'COG1762@1', 'COG1762@2'])

## PGDB Stats:
### Direct Reactions of Protein:
- 
### Complexes of Protein:
--- EIISGC

## Predictions
### Predicted Reactions:
--- RXN0-2522,TRANS-RXN-156,2.7.3.9-RXN,RXN0-2461

Conclusion:
Putative enzyme. EcoCyc has GO terms for the function, but they have computational evidence codes. Again, reaction associations only found at Bacterial level: 'COG1762@2'

%%%

# Report for protein G7093-MONOMER
## Annotation:
Annot(prot_id='G7093-MONOMER', description='UTP:glucose-1-phosphate uridylyltransferase activity', ecs=['2.7.7.9'], eggnog_ids=['1MV5F@1224', '1RMHB@1236', '3XNJT@561', 'COG1210@1', 'COG1210@2'])

## PGDB Stats:
### Direct Reactions of Protein:
- 

## Predictions
### Predicted Reactions:
--- GLUC1PURIDYLTRANS-RXN

Conclusion:

The EcoCyc entry is ambivalent about whether this protein actually catalyzes this function (evidence code for the entry is computational, but the equivalent GO term, here and on the UniProt entry, include an experimental code). Sent email to Ingrid for clarification. I'd call this as a TP, as in a world of possible matches, it picked on that was reasonable.

%%%

# Report for protein EG11134-MONOMER
## Annotation:
Annot(prot_id='EG11134-MONOMER', description='FMN binding', ecs=[''], eggnog_ids=['1PKUV@1224', '1RNQG@1236', '3XNE4@561', 'COG0778@1', 'COG0778@2'])

## PGDB Stats:
### Direct Reactions of Protein:
- 
### Complexes of Protein:
--- CPLX0-7668

## Predictions
### Predicted Reactions:
--- 1.5.1.34-RXN,RXN-8974,RXN-9510,RXN-21128,RXN-21196,RXN-21125,RXN-8081,RXN-21150,RXN-13853,RXN-15785,RXN-20703,RXN-8506,RXN-8815,RXN-12444,NADH-DEHYDROG-A-RXN,RXN-21194,RXN-8080,RXN-21198,RXN-21126,FMNREDUCT-RXN,RXN-8771,R303-RXN,RXN-21200,RXN-21192,R364-RXN

Conclusion:
EcoCyc says putative, with computational evidence. Again, issue with pulling annotations from 'COG0778@2'.

%%%

# Report for protein G7186-MONOMER
## Annotation:
Annot(prot_id='G7186-MONOMER', description='potassium ion transport', ecs=[''], eggnog_ids=['1MU0K@1224', '1RMI1@1236', '3XNMZ@561', 'COG0471@1', 'COG0471@2', 'COG0490@1', 'COG0490@2'])

## PGDB Stats:
### Direct Reactions of Protein:
- 

## Predictions
### Predicted Reactions:
--- TRANS-RXN-127,TRANS-RXN0-201

Conclusion:

EcoCyc says has computational evidence for cation transporter function. Again, found annotations at level of 'COG0471@2'.

%%%

# Report for protein EG12624-MONOMER
## Annotation:
Annot(prot_id='EG12624-MONOMER', description='HTH-type transcriptional regulator YiaG', ecs=[''], eggnog_ids=['1MZTT@1224', '1S95K@1236', '3XPXQ@561', 'COG2944@1', 'COG2944@2'])

## PGDB Stats:
### Direct Reactions of Protein:
- 

## Predictions
### Predicted Reactions:
--- DNA-CYTOSINE-5--METHYLTRANSFERASE-RXN

Conclusion:

Putative enzyme; again most specific OG found at: COG2944@2.

%%%

# Report for protein MONOMER0-4519
## Annotation:
Annot(prot_id='MONOMER0-4519', description='Part of a stress-induced multi-chaperone system, it is involved in the recovery of the cell from heat-induced damage, in cooperation with DnaK, DnaJ and GrpE', ecs=[''], eggnog_ids=['1MURH@1224', '1RN55@1236', '3XN3I@561', 'COG0542@1', 'COG0542@2'])

## PGDB Stats:
### Direct Reactions of Protein:
- 

## Predictions
### Predicted Reactions:
--- 3.4.21.92-RXN

Conclusion:

Again, match found at 'COG0542@2'.

%%%%-END


# Debugging false positives in E. coli

As reported by:

```
pgdb_val_list = pgdb_validate(
   pgdb_env_tuple.annot_ogs, 
    '83333', metacyc_env_tuple.nog2rxn_conn,
    pgdb_env_tuple.taxon2tuple, 
    metacyc_env_tuple.metacyc_predictable_rxns, 
    pgdb_env_tuple.pgdb_testable_reactions, 
    tax_restrict=True,
    per_annot_p=True, 
    annot_struct_dict = pgdb_env_tuple.annot_dict, 
    max_rxns = 10, enclosing_rank='genus')
```

Says that there are 80 FP reactions in EcoCyc.

Looking at this issue from a protein perspective, there are 228 proteins with false positives, as per:

```
eq, fp_fn, fp, fn = categorize_prots(pgdb_env_tuple.annot_dict, pgdb_kb, org_taxon, metacyc_env_tuple.nog2rxn_conn, pgdb_env_tuple.taxon2tuple, enclosing_rank="genus")
```

## List of FP reactions:

%%%

RXN-7911

Predicted for prot: DIENOYLCOAREDUCT-MONOMER

Analysis:
```
>>> pp_annot('DIENOYLCOAREDUCT-MONOMER', pgdb_env_tuple.annot_dict, pgdb_kb, org_taxon, metacyc_env_tuple.nog2rxn_conn, pgdb_env_tuple.taxon2tuple, verbose_p = True, enclosing_rank='genus')
# Report for protein DIENOYLCOAREDUCT-MONOMER
## Annotation:
Annot(prot_id='DIENOYLCOAREDUCT-MONOMER', description='Functions as an auxiliary enzyme in the beta-oxidation of unsaturated fatty acids with double bonds at even carbon positions. Catalyzes the NADPH-dependent reduction of the C4-C5 double bond of the acyl chain of 2,4-dienoyl-CoA to yield 2-trans- enoyl-CoA. Acts on both isomers, 2-trans,4-cis- and 2-trans,4-trans-decadienoyl-CoA, with almost equal efficiency. Is not active with NADH instead of NADPH. Does not show cis- trans isomerase activity', ecs=['1.3.1.34'], eggnog_ids=['1MVE0@1224', '1RNM8@1236', '3XNUV@561', 'COG0446@1', 'COG0446@2', 'COG1902@1', 'COG1902@2'])

## PGDB Stats:
### Direct Reactions of Protein:
- DIENOYLCOAREDUCT-RXN

## Predictions
### Predicted Reactions:
--- DIENOYLCOAREDUCT-RXN,RXN-7911
```
EcoCyc has one associated reaction, whereas the Uniprot entry has three associated Rhea reactions, which map to these two MetaCyc reactions. Sent email to Ingrid on 2020-12-17 to see if she can help me figure out what's going on here.

Ron wrote back on 2020-12-17, saying that the link between Rhea & MetaCyc is incorrect. It doesn't explain the E. coli citations in Rhea for RHEA:12138... wrote them back about that.

%%%

RXN0-7239
Annotations:
- EG12357-MONOMER
- ACYLCOASYN-MONOMER

Would be interesting if the reaction was predicted from two separate proteins, but in both cases it was a genuine FP!

```
# Report for protein EG12357-MONOMER
## Annotation:
Annot(prot_id='EG12357-MONOMER', description='Catalyzes the esterification, concomitant with transport, of exogenous fatty acids into metabolically active CoA thioesters for subsequent degradation or incorporation into phospholipids. Is maximally active on C6 0, C8 0 and C12 0 fatty acids, while has a low activity on C14-C18 chain length fatty acids. Is involved in the anaerobic beta-oxidative degradation of fatty acids, which allows anaerobic growth of E.coli on fatty acids as a sole carbon and energy source in the presence of nitrate or fumarate as a terminal electron acceptor. Can functionally replace FadD under anaerobic conditions', ecs=[''], eggnog_ids=['1MU6G@1224', '1RMQ4@1236', '3XMA2@561', 'COG0318@1', 'COG0318@2'])

## PGDB Stats:
### Direct Reactions of Protein:
- ACYLCOASYN-RXN,TRANS-RXN0-623,RXN0-7238,RXN0-7239,RXN0-7248

## Predictions
### Basis for Predictions:
--- OG: 3XMA2@561
### Predicted Reactions:
--- RXN0-7238,ACYLCOASYN-RXN,TRANS-RXN0-623,RXN0-7239,RXN0-7248
```

```
# Report for protein ACYLCOASYN-MONOMER
## Annotation:
Annot(prot_id='ACYLCOASYN-MONOMER', description='Catalyzes the esterification, concomitant with transport, of exogenous long-chain fatty acids into metabolically active CoA thioesters for subsequent degradation or incorporation into phospholipids', ecs=['6.2.1.3'], eggnog_ids=['1MU6G@1224', '1RMQ4@1236', '3XMU8@561', 'COG0318@1', 'COG0318@2'])

## PGDB Stats:
### Direct Reactions of Protein:
- 
### Complexes of Protein:
--- ACYLCOASYN-CPLX
#### Complex Reactions
---- ACYLCOASYN-RXN,TRANS-RXN0-623,RXN0-7238,RXN0-7239,RXN0-7248

## Predictions
### Basis for Predictions:
--- OG: 3XMU8@561
### Predicted Reactions:
--- RXN0-7238,ACYLCOASYN-RXN,TRANS-RXN0-623,RXN0-7239,RXN-7904,RXN0-7248
```

Nope, this reaction is present in EcoCyc.org! It is an instantiated reaction, but that summary was stripped from the reaction when it was imported into MetaCyc. So my code for coming up with testable reactions falsely thought that this EcoCyc reaction would be unique to EcoCyc, when it fact it was imported. So, the instantiation test needs to be fixed, so that it only throws them out if the reaction is not in MetaCyc.

Sent a note to the BRG for clarification about whether there's been a change in policy, as now all instantiated rxns are imported into MetaCyc. If they all are in MetaCyc now, then I'll stop filtering them out.

Seems like fixing this issue would remove ~12 FPs for EcoCyc.

%%%

RXN-18592 & RXN-185923

Annotation from protein G7420-MONOMER. This protein has no associated reactions in EcoCyc. According to UniProt it does have two associated reactions, but they are from computational prediction only. Indicates that when mapping through UniProt, should only map associations with experimental evidence, or, should make that a toggle for the user.

%%%

2.4.1.129-RXN

%%%

RXN0-4141

%%%

RXN-18593

See RXN-18592

%%%

RXN-18588
PHOSPHOLIPASE-A2-RXN
RXN-6641
CARBOXYMETHYLENEBUTENOLIDASE-RXN
TRANS-RXN0-540
4OH2OXOGLUTARALDOL-RXN
GSHTRAN-RXN
GLYCERATE-DEHYDROGENASE-RXN
D--TARTRATE-DEHYDRATASE-RXN
SPERMINE-SYNTHASE-RXN
DHBAMPLIG-RXN
TRANS-RXN-167
NADPYROPHOSPHAT-RXN
RXN-7904
D-RIBULOKIN-RXN
PSERPHOSPHA-RXN
RXN0-7238
RXN-11190
RXN-8953
RXN-18589
TRANS-RXN-165
CARBODEHYDRAT-RXN
1.2.1.2-RXN
3.1.3.70-RXN
2.6.1.33-RXN
RXN0-5293
RXN-12456
RXN0-2522
RXN-8976
1.1.1.222-RXN
RXN0-7248
HYDROXYPYRUVATE-REDUCTASE-RXN
TRANS-RXN-153A
PHOSPHATIDATE-PHOSPHATASE-RXN
PALMITOYL-COA-HYDROLASE-RXN
RXN-16061
RXN0-17
TRANS-RXN-168
TRANS-RXN-157
TRANS-RXN-167A
RXN-18590
3.6.3.3-RXN
TRANS-RXN-155B
RXN-12160
RXN-10942
TRANS-RXN-169
RXN-12448
RXN-12475
RXN-8946
RXN0-2461
1.1.1.274-RXN
6.3.2.10-RXN
TRANS-RXN-156
1.1.1.8-RXN
4-HYDROXY-2-KETOPIMELATE-LYSIS-RXN
R165-RXN
COBALAMINSYN-RXN
5-METHYLTHIOADENOSINE-PHOSPHORYLASE-RXN
RXN-10949
ACETYL-COA-CARBOXYLTRANSFER-RXN
RXN-12610
RXN-11046
RXN-14063
RXN-18594
RXN-12454
PHOSPHOLIPASE-A1-RXN
RXN-7241
GAPDHSYNEC-RXN
RXN-7929
RXN-18595
ALDHDEHYDROG-RXN
RXN0-6377
3.2.2.10-RXN
ASPARTATE-RACEMASE-RXN
