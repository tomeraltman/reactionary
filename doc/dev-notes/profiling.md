# Benchmarking

## E. coli

### No taxonomic restrictions:

```
Reachability report: 
Reachable OGs:  2193
Out of # predicted OGs: 2193
Predictions using UniProt-EggNOG mapping:
# Known reactions (gold standard):  1428
# predicted reactions:  2421
# Known reactions not predicted:  61
# TP:  1367
# FP:  1054
# FN:  61
Precision:  0.5646427096241222
Recall:     0.9572829131652661
F1 score:   0.7103143673681475
False Positive Report:
# FPs that are 'complex enzyme' reactions: 223
  Sample false positives:
   ['RXN-13627', 'RXN-10903', 'RXN-19568', 'RXN-10669', 'RXN-683', 'RXN-10759', 'RXN-17479', 'CHOLINE-SULFATASE-RXN', 'RXN4FS-2']
False Negative Report:
  Sample false negatives:
   ['ACETOLACTSYN-RXN', 'RXN-17011', 'KETOBUTFORMLY-RXN', 'RXN0-5291', 'RXN-14065', 'CHEBDEAMID-RXN', '2.5.1.25-RXN', 'TRANSENOYLCOARED-RXN', 'RXN-12618']
```

### Taxonomic restrction at Tax-ID 511145 & 83333

No data. Nothing predicted because EggNOG only has OGs starting at Tax-ID 561 (Escherichia).

### Taxonomic restriction at Tax-Id 561

```
Reachability report: 
Reachable OGs:  2193
Out of # predicted OGs: 2193
Predictions using UniProt-EggNOG mapping:
# Known reactions (gold standard):  1428
# predicted reactions:  1353
# Known reactions not predicted:  186
# TP:  1242
# FP:  111
# FN:  186
Precision:  0.917960088691796
Recall:     0.8697478991596639
F1 score:   0.8932038834951457
False Positive Report:
# FPs that are 'complex enzyme' reactions: 89
  Sample false positives:
   ['TRANS-RXN0-540', 'ABC-18-RXN', 'TRANS-RXN0-511', 'TRANS-RXN0-222', 'ABC-36-RXN', 'ABC-26-RXN', 'RXN-8991', 'ABC-42-RXN', 'RXN-8642']
False Negative Report:
  Sample false negatives:
   ['RXN0-2382', 'PHENDEHYD-RXN', 'RXN0-5065', 'RXN0-963', 'DEOXYNUCLEOTIDE-3-PHOSPHATASE-RXN', 'RXN-14473', 'DTMPKI-RXN', 'FADSYN-RXN', 'RXN-17847']
0.9530923366546631
```
### Tax rextriction at Tax-ID 1236 (Gammaproteobacteria)

```
Reachability report: 
Reachable OGs:  2193
Out of # predicted OGs: 2193
Predictions using UniProt-EggNOG mapping:
# Known reactions (gold standard):  1428
# predicted reactions:  1541
# Known reactions not predicted:  68
# TP:  1360
# FP:  181
# FN:  68
Precision:  0.8825438027255029
Recall:     0.9523809523809523
F1 score:   0.9161333782418323
False Positive Report:
# FPs that are 'complex enzyme' reactions: 99
  Sample false positives:
   ['TRANS-RXN-157', 'RXN-17340', '1.3.1.29-RXN', 'TRANS-RXN0-454', 'ABC-70-RXN', 'RXN-17339', 'TRANS-RXN-167A', 'ABC-35-RXN', 'RXN-16909']
False Negative Report:
  Sample false negatives:
   ['RXN-14065', 'ARGSUCCINSYN-RXN', 'RXN0-6461', 'ACETOLACTSYN-RXN', 'PYRUVATEDECARB-RXN', 'RXN-17009', 'PGLYCEROLTRANSII-RXN', 'DTDPDEHYRHAMREDUCT-RXN', 'MCPMETEST-RXN']
1.104374647140503
```

### Tax rextriction at Tax-ID 1224 (Proteobacteria)

```
Reachability report: 
Reachable OGs:  2193
Out of # predicted OGs: 2193
Predictions using UniProt-EggNOG mapping:
# Known reactions (gold standard):  1428
# predicted reactions:  1596
# Known reactions not predicted:  67
# TP:  1361
# FP:  235
# FN:  67
Precision:  0.8527568922305765
Recall:     0.9530812324929971
F1 score:   0.9001322751322751
False Positive Report:
# FPs that are 'complex enzyme' reactions: 112
  Sample false positives:
   ['TRANS-RXN-377', 'D-XYLULOSE-REDUCTASE-RXN', 'RXN-20886', '4-HYDROXY-2-KETOPIMELATE-LYSIS-RXN', 'RXN-13039', '1.2.1.61-RXN', 'TRANS-RXN0-510', 'ABC-42-RXN', 'TRANS-RXN-156']
False Negative Report:
  Sample false negatives:
   ['GLY3KIN-RXN', 'RXN-11152', '2.7.13.2-RXN', 'KETOBUTFORMLY-RXN', 'THYMIDYLATE-5-PHOSPHATASE-RXN', 'RXN0-383', 'RXN0-385', 'RXN-17011', 'RXN0-1882']
```

### Tax rextriction at Tax-ID 2 (Bacteria)

```
Reachability report: 
Reachable OGs:  2193
Out of # predicted OGs: 2193
Predictions using UniProt-EggNOG mapping:
# Known reactions (gold standard):  1428
# predicted reactions:  2421
# Known reactions not predicted:  61
# TP:  1367
# FP:  1054
# FN:  61
Precision:  0.5646427096241222
Recall:     0.9572829131652661
F1 score:   0.7103143673681475
False Positive Report:
# FPs that are 'complex enzyme' reactions: 223
  Sample false positives:
   ['RXN-12860', 'RXN-15249', 'RXN-11753', 'RXN-19528', 'RXN-20119', 'RXN-12772', 'RXN-11493', 'R162-RXN', 'L-IDITOL-2-DEHYDROGENASE-RXN']
False Negative Report:
  Sample false negatives:
   ['GLY3KIN-RXN', 'PROPIONYL-COA-CARBOXY-RXN', 'RXN-14065', 'RXN-17009', 'GALACTONOLACTONASE-RXN', 'RXN0-5184', 'RXN-15312', '1.1.1.168-RXN', 'RXN0-5361']
1.1238126754760742
```


## B. subtilis (BsubCyc )

### TAX-1386: Bacillus

```
Reachability report: 
Reachable OGs:  1197
Out of # predicted OGs: 1197
Predictions using UniProt-EggNOG mapping:
# Known reactions (gold standard):  823
# predicted reactions:  220
# Known reactions not predicted:  649
# TP:  174
# FP:  46
# FN:  649
Precision:  0.7909090909090909
Recall:     0.21142162818955043
F1 score:   0.33365292425695114
False Positive Report:
# FPs that are 'complex enzyme' reactions: 10
  Sample false positives:
   ['RXN-11645', 'DHBAMPLIG-RXN', 'RXN-18018', 'D-XYLULOSE-REDUCTASE-RXN', 'RXN-18029', 'RXN-18061', 'RXN-11298', 'GUANOSINE-DIPHOSPHATASE-RXN', 'RXN-15200']
False Negative Report:
  Sample false negatives:
   ['RR-BUTANEDIOL-DEHYDROGENASE-RXN', 'TSA-REDUCT-RXN', '3-ISOPROPYLMALDEHYDROG-RXN', '6PGLUCONOLACT-RXN', 'CDPKIN-RXN', '3.2.1.10-RXN', 'ACETOOHBUTREDUCTOISOM-RXN', 'DCDPKIN-RXN', 'ALLANTOATE-DEIMINASE-RXN']
1.0175302028656006
```

### TAX-91061: Bacilli

```
Reachability report: 
Reachable OGs:  1197
Out of # predicted OGs: 1197
Predictions using UniProt-EggNOG mapping:
# Known reactions (gold standard):  823
# predicted reactions:  308
# Known reactions not predicted:  609
# TP:  214
# FP:  94
# FN:  609
Precision:  0.6948051948051948
Recall:     0.2600243013365735
F1 score:   0.37842617152961977
False Positive Report:
# FPs that are 'complex enzyme' reactions: 13
  Sample false positives:
   ['6.3.2.7-RXN', 'RXN-15117', 'RXN-21020', 'RXN-8673', 'URPHOS-RXN', 'RXN-20666', 'LACTOSE6P-HYDROXY-RXN', 'RXN-18037', 'RXN-21021']
False Negative Report:
  Sample false negatives:
   ['RXN-9025', 'SPERMACTRAN-RXN', 'RXN-11591', 'RXN0-262', '3.5.2.17-RXN', 'RXN-11633', 'NICOTINAMID-RXN', 'ARYLAMINE-N-ACETYLTRANSFERASE-RXN', '1.2.1.65-RXN']
```

### TAX-1239: Firmicutes

```
Reachability report: 
Reachable OGs:  1197
Out of # predicted OGs: 1197
Predictions using UniProt-EggNOG mapping:
# Known reactions (gold standard):  823
# predicted reactions:  364
# Known reactions not predicted:  595
# TP:  228
# FP:  136
# FN:  595
Precision:  0.6263736263736264
Recall:     0.2770352369380316
F1 score:   0.384161752316765
False Positive Report:
# FPs that are 'complex enzyme' reactions: 23
  Sample false positives:
   ['RXN-12197', 'PROTOCATECHUATE-DECARBOXYLASE-RXN', 'RXN-11106', 'RXN-8674', 'RXN-18030', 'RXN-9220', 'RXN-8568', 'RXN-4961', 'RXN-8672']
False Negative Report:
  Sample false negatives:
   ['RIB5PISOM-RXN', 'PPENTOMUT-RXN', '3.2.1.10-RXN', 'CHORISMATEMUT-RXN', 'RXN0-4261', 'RXN-12458', 'RXN-12002', 'RXN-10814', 'FORMALDEHYDE-DEHYDROGENASE-RXN']
```

### TAX-2: Bacteria

```
Reachability report: 
Reachable OGs:  1197
Out of # predicted OGs: 1197
Predictions using UniProt-EggNOG mapping:
# Known reactions (gold standard):  823
# predicted reactions:  2871
# Known reactions not predicted:  182
# TP:  641
# FP:  2230
# FN:  182
Precision:  0.22326715430163707
Recall:     0.778857837181045
F1 score:   0.3470492690850027
False Positive Report:
# FPs that are 'complex enzyme' reactions: 264
  Sample false positives:
   ['RXN-20927', 'RXN-17380', 'TRANS-RXN-220', 'RXN-20427', 'RXN-21343', 'RXN-17781', 'RXN-21333', 'RXN-9875', 'RXN0-7024']
False Negative Report:
  Sample false negatives:
   ['RXN-1826', 'RXN0-901', '4.4.1.19-RXN', '1.2.1.13-RXN', 'RXN-20679', 'RXN66-3', 'RXN-12188', 'TRIACYLGLYCEROL-LIPASE-RXN', 'RXN-12434']
```


## In MetaCyc & BsubCyc, but not found by Reactionary: 
rxn-12200, CARBPSYN-RXN, RXN-12199,

RXN-12200: BSU30630
not found because too many ambiguous reactions returned

CARBPSYN-RXN
https://biocyc.org/gene?orgid=BSUB&id=BSU11230-MONOMER
https://biocyc.org/gene?orgid=BSUB&id=BSU11240-MONOMER

https://biocyc.org/gene?orgid=BSUB&id=BSU15510-MONOMER
https://biocyc.org/gene?orgid=BSUB&id=BSU15520-MONOMER

RXN-12199
same as RXN12200

