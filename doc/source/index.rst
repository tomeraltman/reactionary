.. Reactionary documentation master file, created by
   sphinx-quickstart on Wed Oct  7 11:22:29 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Reactionary's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Foo
===

Bar

Baz
===

Quux


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
