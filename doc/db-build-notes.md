Built latest database as follows on 2020-11-04:

```
eggnog_rxn_mapping, eggnog_uniprot_mapping, set_diff = map_eggnog_rxn_mapping('/home/taltman/data/reactionary-annot.emapper.annotations', uniprot2rxn)
```

Mapping stats from end of output:

```
Mapping Stats:
Number of UniProt accession numbers harvested from MetaCyc:  200290
Number of MetaCyc UniProt accession numbers in annot file:  199586
Number of MetaCyc UniProt accession numbers with EggNOG mapping(s):  199586
Number of UniProt accessions with EggNOG annotation:  209086
Number of UniProt accessions without direct mapping:  10095
```