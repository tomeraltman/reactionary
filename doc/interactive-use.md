## Minimal working example for running reactionary

### Fetch Reactionary reference DB

From the reactionary home directory execute the following:
```
make fetch-reactionary-db
```
Note the download location, which should be `ref-data/nog2rxn_v6.gz`.


### Import packages

```
from camelot_frs.camelot_frs import get_kb, get_frame
from camelot_frs.pgdb_loader import load_pgdb
from reactionary import reactionary_lib
from reactionary.reactionary_lib import fetch_taxa_of_annotation, fetch_taxon_info
from reactionary.reactionary import augment_eggnog_annotation, coerce_taxonid_to_metacyc_taxon_frame
from reactionary.validate import load_env, load_pgdb_env, pgdb_validate, pwy_comp_report, warpath_vs_pgdb_pwys_report
```

### Load MetaCyc

Assuming that the user keeps his repositories in `/Users/username/repos`:

```
metacyc_pgdb = '/Users/username/Documents/Research/repos/metacyc_24.0_flatfiles/data/'
ref_db_path = '/Users/username/repos/reactionary/ref-data/nog2rxn_v6.gz'
metacyc_env_tuple = load_env(metacyc_pgdb, ref_db_path)
metaKB = get_kb('META')
```

### Load Ecoli annotation information
```
ecoli_taxid = '562'
ecoli_annotation = '/Users/username/Documents/Research/Genome_Annotation/2020-09-28-Reactionary-Benchmarking/annotations/escherichia_coli_K12_MG1655.emapper.annotations'
```

### Parse NCBI taxonomy id
```
reactionary_lib.taxon_db_path = ref_db_path + '/taxa.sqlite'

ncbi_taxon_tuple = fetch_taxon_info([ecoli_taxid])[ecoli_taxid]

taxon2tuple = fetch_taxa_of_annotation(ecoli_annotation)
```

### Run Reactionary
```
augment_eggnog_annotation(ecoli_annotation, taxon2tuple, ncbi_taxon_tuple, metacyc_env_tuple.eggnog2rxns)
```


Reactionary will create a `.reactionary` file in the same folder as the `emapper.annotations` annotation file.

## Advanced

### Validate Reactionary Performance:

Load data for EcoCyc:

```
pgdb_env_list = load_pgdb_env('/home/taltman/data/biocyc-24.0/ecoli/24.0/data', 'ECOLI', ecoli_annotation, metacyc_env_tuple.orphan_dict, metacyc_env_tuple.nog2rxn_conn)
```

