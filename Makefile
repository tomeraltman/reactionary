#### Assumptions:
## * AWS CLI installed & configured
## * sudo apt-get install make python2.7

#### Make configuration:

## Use Bash as default shell, and in strict mode:
SHELL := /bin/bash
.SHELLFLAGS = -ec

## If the parent env doesn't ste TMPDIR, do it ourselves:
TMPDIR ?= /tmp


## This makes all recipe lines execute within a shared shell process:
## https://www.gnu.org/software/make/manual/html_node/One-Shell.html#One-Shell
.ONESHELL:

## If a recipe contains an error, delete the target:
## https://www.gnu.org/software/make/manual/html_node/Special-Targets.html#Special-Targets
.DELETE_ON_ERROR:

## This is necessary to make sure that these intermediate files aren't clobbered:
.SECONDARY:


#### Local Parameters:

## App params:
BUILD_DATA_DIR ?= third-party/data

## Python params:
PYTHON = python3

## eggnog params:
eggnog-mapper-version  := 2.0.0
eggnog-mapper-url      := https://github.com/eggnogdb/eggnog-mapper/archive/$(eggnog-mapper-version).tar.gz
eggnog-mapper-data-dir := $(BUILD_DATA_DIR)
#$(CURDIR)/third-party/eggnog-mapper-$(eggnog-mapper-version)/data

## Reactionary params:
reactionary-version := beta

#### Installation Targets:

install-and-fetch-data: third-party/eggnog-mapper-$(eggnog-mapper-version)/data/foo

install: conda-install install-dev-package
### Python installs:

conda-install:
	conda install -c conda-forge pandoc tectonic
	conda install -c bioconda eggnog-mapper

create-package: clean-package
	$(PYTHON) -m pip install --user --upgrade setuptools wheel twine
	$(PYTHON) setup.py sdist bdist_wheel --universal

clean-package:
	rm -rf dist camelot_frs.egg-info build

install-package: 
	$(PYTHON) -m pip install --user .

install-dev-package: 
	$(PYTHON) -m pip install --user --upgrade -e .

install-dist-package:
	$(PYTHON) -m pip install --user dist/reactionary-0.*-py*-none-any.whl

deploy-package-to-pypi:
	twine upload dist/*


### Third-party installs:

third-party/eggnog-mapper-$(eggnog-mapper-version):
	mkdir -p third-party
	cd third-party \
		&& wget $(eggnog-mapper-url) \
		&& mv $(eggnog-mapper-version).tar.gz eggnog-mapper-$(eggnog-mapper-version).tar.gz \
		&& tar xzf eggnog-mapper-$(eggnog-mapper-version).tar.gz \
		&& rm eggnog-mapper-$(eggnog-mapper-version).tar.gz

third-party/humann2-2.8.1/readme.md:
	$(PYTHON) -m pip install --user --no-binary :all: humann2
	cd third-party \
		&& wget https://files.pythonhosted.org/packages/eb/b4/c6975eb023e915ef999184c040474a781c3f0979d2f1c557fc6e693a7dbb/humann2-2.8.1.tar.gz \
		&& tar xzf humann2-2.8.1.tar.gz

third-party/biosql:
	cd third-party
	git clone git@github.com:biosql/biosql.git

#### Data Staging:

data-setup:
	mkdir -p third-party/data

### EggNOG-mapper
## This still needs Python2:
$(BUILD_DATA_DIR)/eggnog.db: third-party/eggnog-mapper-$(eggnog-mapper-version) data-setup
	cd third-party/eggnog-mapper-$(eggnog-mapper-version)/ \
		&& python ./download_eggnog_data.py -y --data_dir $(eggnog-mapper-data-dir)


## Fetch Reactionary reference DB from FigShare:
fetch-reactionary-db:
	mkdir -p ref-data
	wget "https://ndownloader.figshare.com/files/25440413?private_link=9f94fa9056cdbb4f0b83" \
		-O ref-data/nog2rxn_v6.gz
	@echo "Downloaded to ref-data/nog2rxn_v6.gz"

### Uniprot downloading
dbs/uniprot_sprot.fasta.gz: data-setup
	cd third-party/data
	wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz &
	wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.dat.gz &
	wget http://eggnog5.embl.de/download/eggnog_5.0/id_mappings/uniprot/latest.Bacteria.tsv.gz &
	wait

dbs/rhea2metacyc.tsv: data-setup
	cd third-party/data
	wget ftp://ftp.expasy.org/databases/rhea/tsv/rhea2metacyc.tsv
	wget ftp://ftp.expasy.org/databases/rhea/tsv/rhea%2Ddirections.tsv
	wget ftp://ftp.expasy.org/databases/rhea/tsv/rhea2uniprot%5Fsprot.tsv
	wget ftp://ftp.expasy.org/databases/rhea/tsv/rhea2uniprot%5Ftrembl.tsv.gz
##dbs/uniprot_sprot.dat.gz:

### BioSQL SQLite3 DB building
## Warning: the following requires 
load-ncbi-taxon-db:


### Docker Automation
docker-start:
	sudo systemctl start docker

docker-build: 
	sudo docker build --network=host -t quay.io/taltman/reactionary:$(reactionary-version) .

#docker-pull-from-quay:


docker-run:
	sudo docker run -it --rm --network=host -v /home/taltman/data:/data quay.io/taltman/reactionary:$(reactionary-version) bash 

docker-deploy:
	sudo docker login quay.io
	sudo docker push quay.io/taltman/reactionary



### Tests:

/tmp/test/taxa.sqlite:
	time $(PYTHON) bin/run-reactionary fetch-reference-db /tmp/test

test-ecoli: /tmp/test/taxa.sqlite
	time $(PYTHON) bin/run-reactionary --debug -o /tmp test/test.emapper.annotations 679895 /tmp/test

docker-test-ecoli:
	time python3 bin/run-reactionary test/test.emapper.annotations 679895 user@example.com /root/ref-data/nog2rxn_v6.gz

test: test-ecoli


### Database installs:

dbs/ec2go:
	mkdir -p dbs
	cd dbs && wget http://geneontology.org/external2go/ec2go

dbs/metacyc2go:
	mkdir -p dbs
	cd dbs && wget http://geneontology.org/external2go/metacyc2go

## TODO: Fix this so that it uses a fixed version, so that we can have stable benchmarks:
dbs/uniprot.tsv:
	mkdir -p dbs
	cd dbs \
		&& wget 'http://eggnogdb.embl.de/download/latest/id_mappings/uniprot/latest.gz' \
		&& mv latest.gz uniprot.tsv.gz \
		&& gunzip uniprot.tsv.gz


install-dbs: dbs/ec2go dbs/metacyc2go dbs/uniprot.tsv

get-pputida-seq:
	mkdir -p dbs/p-putida
	cd dbs/p-putida \
		&& wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/007/565/GCF_000007565.2_ASM756v2/GCF_000007565.2_ASM756v2_protein.faa.gz \
		&& gunzip GCF_000007565.2_ASM756v2_protein.faa.gz


get-yeastcyc-prots:
	mkdir -p dbs/sgd
	cd dbs/sgd \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/orf_protein/orf_trans.fasta.gz \
		&& gunzip orf_trans.fasta.gz

get-yeastcyc-seqs:
	mkdir -p dbs/sgd
	cd dbs/sgd \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr01.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr02.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr03.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr04.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr05.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr06.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr07.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr08.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr09.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr10.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr11.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr12.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr13.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr14.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr15.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chr16.fsa \
		&& wget https://downloads.yeastgenome.org/sequence/S288C_reference/NCBI_genome_source/chrmt.fsa \
		&& cat *.fsa > yeastcyc.fsa


get-aracyc-prots:
	mkdir -p dbs/tair
	cd dbs/tair \
		&& wget ftp://ftp.arabidopsis.org/home/tair/Proteins/TAIR10_protein_lists/TAIR10_pep_20101214 -O aracyc-prots.fsa


get-aracyc-seqs:
	mkdir -p dbs/tair
	cd dbs/tair \
		&& wget ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/TAIR10_chr1.fas \
		&& wget ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/TAIR10_chr2.fas \
		&& wget ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/TAIR10_chr3.fas \
		&& wget ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/TAIR10_chr4.fas \
		&& wget ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/TAIR10_chr5.fas \
		&& wget ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/TAIR10_chrC.fas \
		&& wget ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/TAIR10_chrM.fas	\
		&& cat *.fas > aracyc.fsa

get-synel-prots:
	mkdir -p dbs/synel
	cd dbs/synel \
		&& wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/012/525/GCF_000012525.1_ASM1252v1/GCF_000012525.1_ASM1252v1_protein.faa.gz \
		&& gunzip GCF_000012525.1_ASM1252v1_protein.faa.gz

get-bsub-prots:
	mkdir -p dbs/bsub
	cd dbs/bsub \
		&& wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/009/045/GCF_000009045.1_ASM904v1/GCF_000009045.1_ASM904v1_protein.faa.gz \
		&& gunzip GCF_000009045.1_ASM904v1_protein.faa.gz


#### Reference Database Building

### Generate the EggNOG to MetaCyc Reaction mapping file

## Generate a list of SwissProt accessions where the protein has an EggNOG accession shared with at least one MetaCyc-based annotation:
all-swissprot-prots-of-eggnogs:
	awk -F"\t" \
		'NR==FNR { OGs[$$1]; next } \
		 NR!=FNR { FS="; " } \
                 /^AC/ { gsub(/^AC */,""); gsub(/;$/,""); split($$0,accs,FS); next } \
                 /^DR.*eggNOG/ { if($$2 in OGs) for(acc in accs) all_accs[accs[acc]] } \
                 END { for(id in all_accs) print id }' \
		<(zcat reactionary/dbs/nog2rxn_v4.gz) \
		<(zcat dbs/uniprot_sprot.dat.gz) >
		> dbs/all-swissprots-of-metacyc-eggnogs.txt
	awk -F'|' \
		'NR == FNR { accs[$$1]; next } \
		 /^>/ { flag = ($$2 in accs) } \
		 flag' \
		dbs/all-swissprots-of-metacyc-eggnogs.txt \
		<(zcat dbs/uniprot_sprot.fasta.gz) \
		> dbs/all-swissprots-of-metacyc-eggnogs.fasta


annot-all-metacyc-prots:
	mkdir -p dbs/uniprot-metacyc-all
	cd dbs/uniprot-metacyc-all \
		&& time $(PYTHON) \
			../../third-party/eggnog-mapper-$(eggnog-mapper-version)/emapper.py \
			-i ../all-metacyc-uniprot-seqs.fasta \
			-m diamond \
			--output annot \
			--usemem \
			--cpu 15 \
		&& echo $$?

annot-expanded-metacyc-prots-ubc:
	rsync -a third-party /project/st-shallam-1/taltman/
	cp dbs/all-swissprots-of-metacyc-eggnogs.fasta /project/st-shallam-1/taltman/
	qsub src/annotate-swissprot.pbs

# annot-uniprot-metacyc-no-eggnog-archaea:
# 	mkdir -p dbs/uniprot-metacyc-no-eggnog-archaea
# 	cd dbs/uniprot-metacyc-no-eggnog-archaea \
# 		&& 	time python2.7 \
# 			../../third-party/eggnog-mapper-1.0.3/emapper.py \
# 			-i ../uniprot-metacyc-no-eggnog-archaea.fasta \
# 			--output annot \
# 			--usemem \
# 			-d arch \
# 			--cpu 15 \
# 		&& echo $$?

# annot-uniprot-metacyc-no-eggnog-bacteria:
# 	mkdir -p dbs/uniprot-metacyc-no-eggnog-bacteria
# 	cd dbs/uniprot-metacyc-no-eggnog-bacteria \
# 		&& 	time python2.7 \
# 			../../third-party/eggnog-mapper-1.0.3/emapper.py \
# 			-i ../uniprot-metacyc-no-eggnog-bacteria.fasta \
# 			--output annot \
# 			--usemem \
# 			-d bact \
# 			--cpu 15 \
# 		&& echo $$?

# annot-uniprot-metacyc-no-eggnog-eukaryota:
# 	mkdir -p dbs/uniprot-metacyc-no-eggnog-eukaryota
# 	cd dbs/uniprot-metacyc-no-eggnog-eukaryota \
# 		&& 	time python2.7 \
# 			../../third-party/eggnog-mapper-1.0.3/emapper.py \
# 			-i ../uniprot-metacyc-no-eggnog-eukaryota.fasta \
# 			--output annot \
# 			--usemem \
# 			-d euk \
# 			--cpu 15 \
# 		&& echo $$?


test-recipe:
	echo $$foo
	foo=bar
	echo $$foo

sync-emapper-data-to-memory:
	mkdir -p /dev/shm/taltman1/data
	cd third-party/eggnog-mapper-1.0.3/data \
		&& time cp -r ./* /dev/shm/taltman1/data/

test-annotate-ecocyc-prots:
	mkdir -p test/ecocyc-annots
	cd test/ecocyc-annots
	time $(PYTHON) \
			../../third-party/eggnog-mapper-$(eggnog-mapper-version)/emapper.py \
			-i ~/data/ecocyc/23.1/data/protseq.fsa \
			--output annot \
			-m diamond \
			--data_dir /media/storage/emapper-data \
			--usemem \
			--cpu `nproc`

#python2.7 src/test_eggpath.py dbs/metacyc2go \
#		~/repos/barrel-of-monkeys/BoM-output/genomes/hybrid-assembly/annot.emapper.annotations 

test-annotate-yeastcyc-prots:
	mkdir -p test/yeastcyc-annots
	cd test/yeastcyc-annots \
		&& time $(PYTHON) \
			../../third-party/eggnog-mapper-1.0.3/emapper.py \
			-i ../../dbs/sgd/orf_trans.fasta \
			--output annot \
			--usemem \
			-d euk \
			--cpu 15

test-annotate-aracyc-prots:
	mkdir -p test/aracyc-annots
	cd test/aracyc-annots \
		&& time $(PYTHON) \
			../../third-party/eggnog-mapper-1.0.3/emapper.py \
			-i /dev/shm/taltman1/aracyc-prots.fsa \
			--output annot \
			--usemem \
			-d euk \
			--cpu 20 \
		&& echo $$?

test-annotate-pputcyc-prots:
	mkdir -p test/pputcyc-annots
	cd test/pputcyc-annots \
		&& time $(PYTHON) \
			../../third-party/eggnog-mapper-1.0.3/emapper.py \
			-i ../../dbs/p-putida/GCF_000007565.2_ASM756v2_protein.faa \
			--output annot \
			--usemem \
			-d bact \
			--cpu 15 \
		&& echo $$?

test-annotate-bsubcyc-prots:
	mkdir -p test/bsub-annots
	cd test/bsub-annots \
		&& time $(PYTHON) \
			../../third-party/eggnog-mapper-$(eggnog-mapper-version)/emapper.py \
			-i ../../dbs/bsub/GCF_000009045.1_ASM904v1_protein.faa \
			-m diamond \
			--output annot \
			--usemem \
			--cpu 15 \
		&& echo $$?

test-annotate-bsubcyc-prots-bacilli-only:
	mkdir -p test/bsub-annots-bacilli-only
	cd test/bsub-annots-bacilli-only \
		&& time $(PYTHON) \
			../../third-party/eggnog-mapper-1.0.3/emapper.py \
			-i ../../dbs/bsub/GCF_000009045.1_ASM904v1_protein.faa \
			--output annot \
			--usemem \
			-d bacNOG \
			--cpu 15 \
		&& echo $$?


test-annotate-synelcyc-prots:
	mkdir -p test/synel-annots
	cd test/synel-annots \
		&& time $(PYTHON) \
			../../third-party/eggnog-mapper-1.0.3/emapper.py \
			-i ../../dbs/synel/GCF_000012525.1_ASM1252v1_protein.faa \
			--output annot \
			--usemem \
			-d bact \
			--cpu 15


test-annotate-genomes: test-annotate-pputcyc-prots test-annotate-aracyc-prots test-annotate-yeastcyc-prots


### Testing annotation of all of SwissProt:

## 43 proteins that EggNOG-Mapper didn't return any results for:
## Not sure why. Can re-run EggNOG-Mapper on these, or run them manually via EggNOG website.
uniprot-accs-no-annot:
	comm -23 <(awk '!/^#/ && /^>/ { gsub(/^>/,""); print $$1}' ./all-swissprots-of-metacyc-eggnogs.fasta| sort ) <(awk -F"\t" '!/^#/ { print $$1}' ~/scratch/taltman/annot.emapper.annotations | sort ) | wc -l

## Spot-check the proteins that I added to the MetaCyc UniProt set:
## P43807 is in expanded set, and not in MetaCyc.
## >sp|P43807|RNH_HAEIN Ribonuclease HI OS=Haemophilus influenzae (strain ATCC 51907 / DSM 11121 / KW20 / Rd) OX=71421 GN=rnhA PE=3 SV=1
## As annotated by EggNOG:
## sp|P43807|RNH_HAEIN	71421.HI_0138	4.2e-88	330.5	Pasteurellales	rnhA	GO:0003674,GO:0003824,GO:0004518,GO:0004519,GO:0004521,GO:0004523,GO:0004540,GO:0006139,GO:0006259,GO:0006260,GO:0006261,GO:0006271,GO:0006273,GO:0006401,GO:0006725,GO:0006807,GO:0008150,GO:0008152,GO:0009056,GO:0009057,GO:0009058,GO:0009059,GO:0009987,GO:0016070,GO:0016787,GO:0016788,GO:0016891,GO:0016893,GO:0019439,GO:0022616,GO:0033567,GO:0034641,GO:0034645,GO:0034655,GO:0043137,GO:0043170,GO:0044237,GO:0044238,GO:0044248,GO:0044249,GO:0044260,GO:0044265,GO:0044270,GO:0046483,GO:0046700,GO:0071704,GO:0090304,GO:0090305,GO:0090501,GO:0090502,GO:0140098,GO:1901360,GO:1901361,GO:1901575,GO:1901576	3.1.26.4	ko:K03469	ko03030,map03030				ko00000,ko00001,ko01000,ko03032				Bacteria	1RCZ1@1224,1S3YC@1236,1Y8FK@135625,COG0328@1,COG0328@2	NA|NA|NA	L	Endonuclease that specifically degrades the RNA of RNA- DNA hybrids
## Verified, checks out! Matches RnhA gene in E. coli, same top-level NOGs
## 
## Next:
## >sp|A0QSU4|Y1603_MYCS2 Uncharacterized oxidoreductase MSMEG_1603/MSMEI_1564 OS=Mycolicibacterium smegmatis (strain ATCC 700084 / mc(2)155) OX=246196 GN=MSMEG_1603 PE=1 SV=1
## Maps to : 
## >sp|P0ADG5|SUHB_ECOL6 Inositol-1-monophosphatase OS=Escherichia coli O6:H1 (strain CFT073 / ATCC 700928 / UPEC) OX=199310 GN=suhB PE=3 SV=1
## sp|A0QSU4|Y1603_MYCS2	246196.MSMEI_1564	1e-204	719.2	Mycobacteriaceae	guaB	GO:0005575,GO:0005622,GO:0005623,GO:0005737,GO:0005886,GO:0006139,GO:0006163,GO:0006164,GO:0006183,GO:0006725,GO:0006753,GO:0006793,GO:0006796,GO:0006807,GO:0008150,GO:0008152,GO:0009058,GO:0009116,GO:0009117,GO:0009119,GO:0009141,GO:0009142,GO:0009144,GO:0009145,GO:0009150,GO:0009152,GO:0009163,GO:0009165,GO:0009199,GO:0009201,GO:0009205,GO:0009206,GO:0009259,GO:0009260,GO:0009987,GO:0016020,GO:0018130,GO:0019438,GO:0019637,GO:0019693,GO:0034404,GO:0034641,GO:0034654,GO:0042278,GO:0042451,GO:0042455,GO:0044237,GO:0044238,GO:0044249,GO:0044271,GO:0044281,GO:0044283,GO:0044424,GO:0044464,GO:0046039,GO:0046128,GO:0046129,GO:0046390,GO:0046483,GO:0055086,GO:0071704,GO:0071944,GO:0072521,GO:0072522,GO:0090407,GO:1901068,GO:1901070,GO:1901135,GO:1901137,GO:1901293,GO:1901360,GO:1901362,GO:1901564,GO:1901566,GO:1901576,GO:1901657,GO:1901659	1.1.1.205	ko:K00088	ko00230,ko00983,ko01100,ko01110,map00230,map00983,map01100,map01110	M00050	R01130,R08240	RC00143,RC02207	ko00000,ko00001,ko00002,ko01000,ko04147				Bacteria	234K3@1762,2GKVS@201174,COG0516@1,COG0516@2	NA|NA|NA	F	IMP dehydrogenase

